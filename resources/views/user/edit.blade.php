@extends('master/layout')

@section('content')
<style>
    .uper {
        margin-top: 40px;
    }
</style>
<div class="card uper">
    <div class="card-header">
        Edit User
    </div>
    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
        @endif
        <form method="post" action="{{ route('users.update', $user->id) }}">
            <div class="form-group">
                @csrf
                @method('PATCH')
                <label for="name">First Name:</label>
                <input type="text" class="form-control" name="first_name" value="{{$user->first_name}}"/>
            </div>
            <div class="form-group">
                <label for="price">Last Name :</label>
                <input type="text" class="form-control" name="last_name" value="{{$user->last_name}}"/>
            </div>
            <div class="form-group">
                <label for="quantity">Email :</label>
                <input type="text" class="form-control" name="email" value="{{$user->email}}"/>
            </div>
            <button type="submit" class="btn btn-primary">Update User</button>
        </form>
    </div>
</div>
@endsection