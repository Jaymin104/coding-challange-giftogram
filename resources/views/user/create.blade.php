@extends('master/layout')

@section('content')
<style>
    .uper {
        margin-top: 40px;
    }
</style>
<div class="card uper">
    <div class="card-header">
        Register User
    </div>
    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
        @endif
        <form method="post" action="{{ route('users.store') }}">
            <div class="form-group">
                @csrf
                <label for="name">User First Name:</label>
                <input type="text" class="form-control" name="first_name"/>
            </div>
            <div class="form-group">
                <label for="price">User Last Name :</label>
                <input type="text" class="form-control" name="last_name"/>
            </div>
            <div class="form-group">
                <label for="quantity">User Email :</label>
                <input type="text" class="form-control" name="email"/>
            </div>
            <div class="form-group">
                <label for="quantity">User Password :</label>
                <input type="password" class="form-control" name="password"/>
            </div>
            <button type="submit" class="btn btn-primary">Register User</button>
        </form>
    </div>
</div>
@endsection