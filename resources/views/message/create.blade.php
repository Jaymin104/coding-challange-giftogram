@extends('master/layout')

@section('content')
<style>
    .uper {
        margin-top: 40px;
    }
</style>
<div class="card uper">
    <div class="card-header">
        Start Chatting
    </div>
    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
        @endif
        <h4>Chat To: {{$user->first_name}} </h4>
        <form method="post" action="{{ route('messages.store') }}">
            <div class="form-group">
                @csrf
                <label for="name">Write Message:</label>
                <input type="text" class="form-control" name="message"/>
                <input type="hidden" name="receiver_user_id" value="{{$user->id}}"/>
                <input type="hidden" name="sender_user_id" value="1"/>
            </div>
            <button type="submit" class="btn btn-primary">Send Message</button>
        </form>
    </div>
</div>
@endsection