@extends('master/layout')

@section('content')
<style>
    .uper {
        margin-top: 40px;
    }
</style>
<div class="uper">
    @foreach($senderDetail as $senderDetails)
    <h3>Chatting History between <b>{{$senderDetails->first_name}}</b> &
    @endforeach
    
    @foreach($receiverDetail as $receiverDetails)
    <b>{{$receiverDetails->first_name}}</b></h3>
    @endforeach
    
    @if(session()->get('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}  
    </div><br />
    @endif
    <table class="table table-striped">
        <thead>
            <tr>
                <td>Sender</td>
                <td>Message</td>
                <td colspan="2">Timestamp</td>
            </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
            <tr>
                <td>{{$user->first_name}}</td>
                <td>{{$user->message}}</td>
                <td>{{$user->epoch}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div>
        @endsection