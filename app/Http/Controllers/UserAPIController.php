<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Response;
use DB;
use Illuminate\Support\Facades\Validator;
use Hash;
use Auth;

class UserAPIController extends Controller {

    //Login API
    public function login() {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = Auth::user();
            //$success['token'] =  $user->createToken('MyApp')-> accessToken; 
            $data = array(
                'user_id' => $user->id,
                'email' => $user->email,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name
            );
            return response()->json($data);
        } else {
            return response()->json(['error' => 'You are Unauthorised for this system'], 401);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
                    'first_name' => 'required|max:100',
                    'last_name' => 'required|max:100',
                    'email' => 'required|email|unique:users',
                    'password' => 'required|min:6'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->messages(), 201);
        } else {
            $requestData = array(
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'first_name' => $request->first_name,
                'last_name' => $request->last_name
            );
            User::create($requestData);
            $user_id = DB::getPdo()->lastInsertId();
            $data = array(
                'user_id' => $user_id,
                'email' => $request->email,
                'first_name' => $request->first_name,
                'last_name' => $request->last_name
            );
            return Response::json($data);
        }
    }

    //List All Users excluding the requester
    public function listAllUser(Request $request) {
        $request_id = $request->requester_user_id;
        $result = DB::table('users')
                ->where('id', '!=', $request_id)
                ->get(array(
            'id as user_id',
            'email',
            'first_name',
            'last_name'
        ));
        return response()->json(['users' => $result]);
    }

}
