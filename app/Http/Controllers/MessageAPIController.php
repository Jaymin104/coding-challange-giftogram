<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Response;
use Illuminate\Support\Facades\Validator;

class MessageAPIController extends Controller {

    //Send Message
    public function sendMessage(Request $request) {
        //$request->request->add(['chat_id' => $request->sender_user_id . '_' . $request->receiver_user_id]);
        $validator = Validator::make($request->all(), [
                    'sender_user_id' => 'required',
                    'receiver_user_id' => 'required',
                    'message' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->messages(), 201);
        } else {
            $epoch = strtotime(Carbon::now());
            $request->request->add(['epoch' => $epoch]); //add request
            DB::table('messages')->insert($request->all());
            return Response::json(array(
                        'success_code' => 200,
                        'success_title' => 'Message Sent',
                        'success_message' => 'Message was sent successfully!!',
            ));
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
//    public function viewMessage($sender_user_id, $receiver_user_id) {
//        $chat_id = $sender_user_id . '_' . $receiver_user_id;
//        $chat_id_rev = $receiver_user_id . '_' . $sender_user_id;
//        $result = DB::table('messages')
//                ->where('chat_id', '=', $chat_id)
//                ->orWhere('chat_id', '=', $chat_id_rev)
//                ->get();
//        return response()->json(['messages' => $result]);
//    }

    //View Messages between two users. Above is little bit different idea but its slow comparing with this one.
    public function viewMessage(Request $request) {
        $senderUserId = $request->user_id_a;
        $receiverUserId = $request->user_id_b;
        $result = DB::table('messages')
                ->whereIn('sender_user_id', array($senderUserId, $receiverUserId))
                ->whereIn('receiver_user_id', array($senderUserId, $receiverUserId))
                ->orderBy('epoch', 'DESC')
                ->get(array(
                    'id as message_id',
                    'sender_user_id',
                    'message',
                    'epoch'
                ));
        if ($result->isEmpty()) {
            return response()->json(['messages' => 'There is no chatting between these two users']);
        } else {
            return response()->json(['messages' => $result]);
        }
    }

}
