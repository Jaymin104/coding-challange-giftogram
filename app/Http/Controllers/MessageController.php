<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class MessageController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
                    'sender_user_id' => 'required',
                    'receiver_user_id' => 'required',
                    'message' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect('/users')->with('Error', 'Message Not Delivered');
        } else {
            $epoch = strtotime(Carbon::now());
            $data = array(
                'sender_user_id' => $request->sender_user_id,
                'receiver_user_id' => $request->receiver_user_id,
                'message' => $request->message,
                'epoch' => $epoch
            );
            DB::table('messages')->insert($data);
            return redirect('/users')->with('success', 'Message Delivered successfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $users = DB::table('messages')
                ->join('users', 'messages.sender_user_id', '=', 'users.id')
                ->whereIn('sender_user_id', array(1, $id))
                ->whereIn('receiver_user_id', array(1, $id))
                ->orderBy('epoch', 'DESC')
                ->get(array(
                    'messages.id as message_id',
                    'first_name',
                    'sender_user_id',
                    'receiver_user_id',
                    'message',
                    'epoch'
                ));
                //print_r($users);
                //die();
        $receiverDetail = DB::table('users')
                ->where('id', $id)
                ->get();
        $senderDetail = DB::table('users')
                ->where('id', 1)
                ->get();
        return view('message/index', compact('users', 'receiverDetail', 'senderDetail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $user = User::findOrFail($id);
        return view('message/create', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
