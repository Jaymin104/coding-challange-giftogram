<?php

namespace App;

use Eloquent as Model;

/**
 * Class Book
 * @package App\Models
 * @version September 17, 2018, 1:00 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection GameNumber
 * @property \Illuminate\Database\Eloquent\Collection Inventory
 * @property \Illuminate\Database\Eloquent\Collection roleHasPermissions
 * @property \Illuminate\Database\Eloquent\Collection storeUsers
 * @property \Illuminate\Database\Eloquent\Collection userShifts
 * @property string game
 * @property float book_price
 * @property integer ticket_qty
 * @property float ticket_price
 * @property string image
 */
class Message extends Model
{

    public $table = 'messages';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    public $fillable = [
        'sender_user_id',
        'receiver_user_id',
        'message'
    ];

    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
}
