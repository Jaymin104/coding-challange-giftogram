<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user(); 
});

//Register User
Route::post('register', 'UserAPIController@store');
//User Login
Route::post('login', 'UserAPIController@login');
//Send message to other user
Route::post('send_messages', 'MessageAPIController@sendMessage');
//View messages between two users
Route::get('view_messages', 'MessageAPIController@viewMessage');
//List all users excluding given id
Route::get('list_all_users', 'UserAPIController@listAllUser');


//Extra API
//Update user

